# coding: utf-8

RESOURCE_MAPPING = {

    # Senadores
    'senadores': {
        'resource': 'senador/lista/atual',
        'docs': 'http://legis.senado.gov.br/dadosabertos/docs/ui/index.html#!/ListaSenadorService/resource_ListaSenadorService_listaSenadoresXml_GET',
        'methods': ['GET']
    },
}
